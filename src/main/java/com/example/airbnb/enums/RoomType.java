package com.example.airbnb.enums;

public enum RoomType {
    SINGLE, DOUBLE, SUITE, STUDIO, DELUXE, SUPER_DELUXE
}
