package com.example.airbnb.entity;

import com.example.airbnb.enums.RoomType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "rooms")
@Builder
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Guest owner;
    private Double price;
    private boolean hotTub;
    @Enumerated(EnumType.STRING)
    private RoomType roomType;
    private int totalOccupancy;
    private int totalBathrooms;
    private String summary;
    private Date createdAt;
    private Date updatedAt;
}
