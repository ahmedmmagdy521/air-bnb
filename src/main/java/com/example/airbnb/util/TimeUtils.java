package com.example.airbnb.util;

import com.example.airbnb.exception.CustomException;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

@UtilityClass
public class TimeUtils {

    public static final ZoneId UTC = ZoneId.of("UTC");

    public static Integer getNumberOfDays(LocalDate startDate, LocalDate endDate) {
        return Math.toIntExact(endDate.toEpochDay() - startDate.toEpochDay()) + 1;
    }

    public static Integer getNumberOfDays(Long startDate, Long endDate) {
        return getNumberOfDays(LocalDate.ofInstant(Instant.ofEpochMilli(startDate), UTC)
                , LocalDate.ofInstant(Instant.ofEpochMilli(endDate), UTC));
    }

    public static void validateTimeRange(Long startDate, Long endDate) {
        if (endDate <= startDate)
            throw new CustomException("time.invalid", HttpStatus.BAD_REQUEST);
        if (LocalDate.ofInstant(Instant.ofEpochMilli(startDate), UTC).isBefore(LocalDate.now()))
            throw new CustomException("time.start.invalid", HttpStatus.BAD_REQUEST);
    }
}
