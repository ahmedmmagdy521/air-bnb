package com.example.airbnb.exception;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class RestExceptionHandler {
    private final MessageSource messageSource;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<ErrorMessage> handleConstraintViolation(ConstraintViolationException ex) {
        String message = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
                .collect(Collectors.joining());
        ErrorMessage errorBody = new ErrorMessage(message);
        log.error(message);
        return new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<ErrorMessage> handleValidationException(ValidationException ex) {
        String message = ex.getMessage();
        ErrorMessage errorBody = new ErrorMessage(message);
        log.error(message);
        return new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<ErrorMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        ErrorsField errorsField = new ErrorsField(ex.getFieldError().getDefaultMessage(),
                ex.getFieldError().getField(),
                ex.getFieldError().getObjectName());
        String message = errorsField.toString();
        ErrorMessage errorBody = new ErrorMessage(message);
        log.error(message);
        return new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(CustomException.class)
    protected ResponseEntity<ErrorMessage> handleCustomException(CustomException ex, Locale locale) {
        String message = messageSource.getMessage(ex.getCode(), ex.getArgs(), locale);
        ErrorMessage errorBody = new ErrorMessage(message);
        log.error(message);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept-Charset","UTF-8");
        headers.add("Accept-Encoding","UTF-8");
        return new ResponseEntity<>(errorBody,headers, ex.getStatus());
    }
}
