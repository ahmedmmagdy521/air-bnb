package com.example.airbnb.exception;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
    public ErrorMessage(String message) {
        this.message = message;
    }

    String message;
    Date time = new Date();
}
