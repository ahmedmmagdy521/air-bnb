package com.example.airbnb.exception;

public record ErrorsField(String message, String field, String className) {
    @Override
    public String toString() {
        return className + "." + field + " " + message;
    }
}
