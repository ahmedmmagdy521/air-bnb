package com.example.airbnb.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomException extends RuntimeException {
    private String code;
    private HttpStatus status;
    private Object[] args;

    public CustomException(String code, HttpStatus status) {
        this.code = code;
        this.status = status;
        this.args = new Object[]{};
    }
}
