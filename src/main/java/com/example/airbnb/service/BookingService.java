package com.example.airbnb.service;

import com.example.airbnb.entity.Booking;
import com.example.airbnb.entity.Guest;
import com.example.airbnb.entity.Room;
import com.example.airbnb.exception.CustomException;
import com.example.airbnb.model.PageResult;
import com.example.airbnb.repository.BookingRepository;
import com.example.airbnb.util.TimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookingService {
    public static final ZoneId UTC = ZoneId.of("UTC");
    public static final int MIN_BOOKING_DAYS = 1;
    public static final int MAX_BOOKING_DAYS = 30;
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String ROOM = "room";
    private final BookingRepository repository;
    @Lazy
    @Autowired
    private RoomService roomService;

    public Booking createBooking(Guest guest, Room room, Long startDate, Long endDate) {
        TimeUtils.validateTimeRange(startDate, endDate);
        int numberOfDays = TimeUtils.getNumberOfDays(startDate, endDate);
        if (numberOfDays < MIN_BOOKING_DAYS || numberOfDays > MAX_BOOKING_DAYS)
            throw new CustomException("booking.days.invalid", HttpStatus.BAD_REQUEST);
        Booking booking = initializeBooking(guest, room, startDate, endDate, numberOfDays);
        checkIfBookingIsAvailable(booking);
        return repository.save(booking);
    }

    private void checkIfBookingIsAvailable(Booking booking) {
        Specification<Booking> spec = Specification.where((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(ROOM), Room.builder().id(booking.getRoom().getId()).build()));
        spec = spec.and(Specification.where((root, query, criteriaBuilder) ->
                criteriaBuilder.between(root.get(START_DATE), booking.getStartDate(), booking.getEndDate())));
        spec = spec.and(Specification.where((root, query, criteriaBuilder) ->
                criteriaBuilder.between(root.get(END_DATE), booking.getStartDate(), booking.getEndDate())));
        boolean notAvailable = repository.exists(spec);
        if (notAvailable)
            throw new CustomException("booking.not_available", HttpStatus.NOT_ACCEPTABLE);
    }

    private Booking initializeBooking(Guest guest, Room room, Long startDate, Long endDate, int numberOfDays) {
        Booking booking = Booking.builder().createdAt(new Date())
                .startDate(LocalDate.ofInstant(Instant.ofEpochMilli(startDate), UTC))
                .endDate(LocalDate.ofInstant(Instant.ofEpochMilli(endDate), UTC))
                .price(room.getPrice()).guest(guest).room(room).build();
        booking.setPrice(room.getPrice() * numberOfDays);
        return booking;
    }

    public Booking getBookingById(Long id) {
        return repository.findById(id).orElseThrow(() -> new CustomException("booking.not_found", HttpStatus.NOT_FOUND));
    }

    public void cancelBooking(Guest guest, Long bookingId) {
        Booking booking = getBookingById(bookingId);
        LocalDate now = LocalDate.now(UTC);
        LocalDate startDate = booking.getStartDate();
        LocalDate endDate = booking.getEndDate();
        if (endDate.isBefore(now) || startDate.isBefore(now))
            throw new CustomException("booking.cancel.not_accepted", HttpStatus.BAD_REQUEST);
        if (!booking.getGuest().getId().equals(guest.getId()))
            throw new CustomException("booking.delete.forbidden", HttpStatus.FORBIDDEN);
        if ((startDate.equals(now) || startDate.isBefore(now)) && (endDate.equals(now) || endDate.isAfter(now)))
            roomService.removeOwner(booking.getRoom().getId());
        repository.deleteById(bookingId);
    }

    public PageResult getRoomBookings(Long roomId, Boolean active, Pageable pageable) {
        Specification<Booking> spec = Specification.where((root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ROOM), Room.builder().id(roomId).build()));
        if (Boolean.TRUE.equals(active))
            spec = spec.and(Specification.where((root, query, criteriaBuilder) ->
                    criteriaBuilder.greaterThanOrEqualTo(root.get(END_DATE), LocalDate.now(UTC))));
        long count = repository.count(spec);
        if (count == 0L)
            return PageResult.builder().totalCount(0L).result(List.of()).build();
        List<Booking> bookingsPage = repository.findAll(spec, pageable).getContent();
        return PageResult.builder().totalCount(count).result(bookingsPage).build();
    }
}
