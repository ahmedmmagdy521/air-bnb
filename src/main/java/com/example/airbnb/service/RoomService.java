package com.example.airbnb.service;

import com.example.airbnb.dto.RoomDto;
import com.example.airbnb.entity.Booking;
import com.example.airbnb.entity.Guest;
import com.example.airbnb.entity.Room;
import com.example.airbnb.enums.RoomType;
import com.example.airbnb.exception.CustomException;
import com.example.airbnb.mapper.RoomMapper;
import com.example.airbnb.model.PageResult;
import com.example.airbnb.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoomService {
    public static final String HOT_TUB = "hotTub";
    public static final String ROOM_TYPE = "roomType";
    public static final String TOTAL_BATHROOMS = "totalBathrooms";
    public static final String TOTAL_OCCUPANCY = "totalOccupancy";
    public static final String PRICE = "price";
    public static final ZoneId UTC = ZoneId.of("UTC");
    private final RoomMapper mapper;
    private final RoomRepository repository;
    private final BookingService bookingService;

    public PageResult getAllRooms(Pageable page, Boolean hotTub, RoomType roomType, Integer totalBathrooms, Integer totalOccupancy, Double price) {
        Specification<Room> spec = Specification.where(null);
        spec = addEqualsCriteria(HOT_TUB, hotTub, spec);
        spec = addEqualsCriteria(ROOM_TYPE, roomType, spec);
        spec = addEqualsCriteria(TOTAL_BATHROOMS, totalBathrooms, spec);
        spec = addEqualsCriteria(TOTAL_OCCUPANCY, totalOccupancy, spec);
        spec = addLessThanCriteria(PRICE, price, spec);
        long count = repository.count(spec);
        if (count == 0L)
            return PageResult.builder().totalCount(0L).result(List.of()).build();
        List<RoomDto> roomsPage = repository.findAll(spec, page).getContent().stream().map(mapper::toDto).collect(Collectors.toList());
        return PageResult.builder().totalCount(count).result(roomsPage).build();
    }

    private Specification<Room> addEqualsCriteria(String key, Object value, Specification<Room> spec) {
        if (value != null) {
            spec = spec.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get(key), value));
        }
        return spec;
    }

    private Specification<Room> addLessThanCriteria(String key, Double value, Specification<Room> spec) {
        if (value != null) {
            spec = spec.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.le(root.get(key), value));
        }
        return spec;
    }

    public RoomDto getRoomDtoById(Long id) {
        return mapper.toDto(getRoomById(id));
    }

    public Room getRoomById(Long id) {
        return repository.findById(id).orElseThrow(() -> new CustomException("room.not_found", HttpStatus.NOT_FOUND));
    }

    public Room createRoom(RoomDto dto) {
        Room room = mapper.toEntity(dto);
        return repository.save(room);
    }

    public Room updateRoom(Long id, RoomDto dto) {
        Room existingRoom = getRoomById(id);
        Room updatedRoom = mapper.updateEntity(existingRoom, dto);
        return repository.save(updatedRoom);
    }

    public void deleteRoom(Long id) {
        repository.deleteById(id);
    }

    public Booking createBooking(Guest guest, Long roomId, Long startDate, Long endDate) {
        Room room = getRoomById(roomId);
        if (LocalDate.ofInstant(Instant.ofEpochMilli(startDate), UTC).equals(LocalDate.now(UTC))) {
            room.setOwner(guest);
            repository.save(room);
        }
        return bookingService.createBooking(guest, room, startDate, endDate);
    }

    public void removeOwner(Long id) {
        Room room = getRoomById(id);
        room.setOwner(null);
        repository.save(room);
    }

    public PageResult getRoomBookings(Long id,Boolean active,Pageable pageable) {
        return bookingService.getRoomBookings(id,active,pageable);
    }
}
