package com.example.airbnb.service;

import com.example.airbnb.dto.GuestDto;
import com.example.airbnb.entity.Booking;
import com.example.airbnb.entity.Guest;
import com.example.airbnb.exception.CustomException;
import com.example.airbnb.mapper.GuestMapper;
import com.example.airbnb.model.PageResult;
import com.example.airbnb.repository.GuestRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GuestService {
    public static final String NAME = "name";
    public static final String COUNTRY = "country";
    public static final String EMAIL = "email";
    public static final String LIKE_OP = "%";

    private final GuestMapper mapper;
    private final GuestRepository repository;
    private final RoomService roomService;
    private final BookingService bookingService;

    public PageResult getAllGuests(Pageable page, String name, String country, String email) {
        Specification<Guest> spec = Specification.where(null);
        spec = addLikeCriteria(NAME, name, spec);
        spec = addLikeCriteria(COUNTRY, country, spec);
        spec = addLikeCriteria(EMAIL, email, spec);
        long count = repository.count(spec);
        if (count == 0L)
            return PageResult.builder().totalCount(0L).result(List.of()).build();
        List<GuestDto> guestsPage = repository.findAll(spec, page).getContent().stream().map(mapper::toDto).collect(Collectors.toList());
        return PageResult.builder().totalCount(count).result(guestsPage).build();
    }

    private Specification<Guest> addLikeCriteria(String key, String value, Specification<Guest> spec) {
        if (value != null && !value.isBlank()) {
            spec = spec.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.like(root.get(key), LIKE_OP + value.trim() + LIKE_OP));
        }
        return spec;
    }

    public GuestDto getGuestDtoById(Long id) {
        return mapper.toDto(getGuestById(id));
    }

    public Guest getGuestById(Long id) {
        return repository.findById(id).orElseThrow(() -> new CustomException("guest.not_found", HttpStatus.NOT_FOUND));
    }

    public Guest createGuest(GuestDto dto) {
        Guest guest = mapper.toEntity(dto);
        checkIfGuestExists(guest);
        return repository.save(guest);
    }

    private void checkIfGuestExists(Guest guest) {
        Specification<Guest> spec = Specification.where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(EMAIL), guest.getEmail())
        );
        boolean exists = repository.exists(spec);
        if (exists)
            throw new CustomException("guest.conflict", HttpStatus.CONFLICT);
    }

    public Guest updateGuest(Long id, GuestDto dto) {
        Guest existingGuest = getGuestById(id);
        Guest updatedGuest = mapper.updateEntity(existingGuest, dto);
        return repository.save(updatedGuest);
    }

    public void deleteGuest(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    public Booking bookRoom(Long id, Long roomId, Long startDate, Long endDate) {
        Guest guest = getGuestById(id);
        return roomService.createBooking(guest, roomId, startDate, endDate);
    }

    @Transactional
    public void cancelBooking(Long id, Long bookingId) {
        Guest guest = getGuestById(id);
        bookingService.cancelBooking(guest, bookingId);
    }
}
