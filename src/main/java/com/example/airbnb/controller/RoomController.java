package com.example.airbnb.controller;

import com.example.airbnb.dto.RoomDto;
import com.example.airbnb.entity.Room;
import com.example.airbnb.enums.RoomType;
import com.example.airbnb.model.PageResult;
import com.example.airbnb.service.RoomService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(RoomController.PATH)
public class RoomController {
    public static final String PATH = "v1/rooms";
    public static final String START_DATE = "startDate";
    private final RoomService service;

    @GetMapping
    public PageResult getAllRooms(@RequestParam(required = false) Boolean hotTub,
                                  @RequestParam(required = false) RoomType roomType,
                                  @RequestParam(required = false) Integer totalBathrooms,
                                  @RequestParam(required = false) Integer totalOccupancy,
                                  @RequestParam(required = false) Double price,
                                  @RequestParam(required = false, defaultValue = "10") int size,
                                  @RequestParam(required = false, defaultValue = "0") int page,
                                  @RequestParam(required = false, defaultValue = "price") String sortBy,
                                  @RequestParam(required = false, defaultValue = "ASC") Sort.Direction direction
    ) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.by(direction, sortBy));
        return service.getAllRooms(pageRequest, hotTub, roomType, totalBathrooms, totalOccupancy, price);
    }

    @GetMapping("/{id}")
    public RoomDto getRoomById(@PathVariable("id") Long id) {
        return service.getRoomDtoById(id);
    }

    @PostMapping
    public Room createRoom(@RequestBody @Valid RoomDto guest) {
        return service.createRoom(guest);
    }

    @PutMapping("/{id}")
    public Room updateRoom(@PathVariable("id") Long id, @RequestBody @Valid RoomDto dto) {
        return service.updateRoom(id, dto);
    }

    @DeleteMapping("/{id}")
    public void deleteRoom(@PathVariable("id") Long id) {
        service.deleteRoom(id);
    }

    @GetMapping("/{id}/bookings")
    public PageResult getRoomBookings(@PathVariable("id") Long id,
                                         @RequestParam(required = false, defaultValue = "100") int size,
                                         @RequestParam(required = false, defaultValue = "0") int page,
                                         @RequestParam(required = false, defaultValue = "true") Boolean active) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, START_DATE));
        return service.getRoomBookings(id, active, pageable);
    }

}
