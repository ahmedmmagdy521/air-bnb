package com.example.airbnb.controller;

import com.example.airbnb.dto.GuestDto;
import com.example.airbnb.entity.Booking;
import com.example.airbnb.entity.Guest;
import com.example.airbnb.model.PageResult;
import com.example.airbnb.service.GuestService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(GuestController.PATH)
public class GuestController {
    public static final String PATH = "v1/guests";
    private final GuestService service;

    @GetMapping
    public PageResult getAllGuests(@RequestParam(required = false) String name,
                                   @RequestParam(required = false) String country,
                                   @RequestParam(required = false) String email,
                                   @RequestParam(required = false, defaultValue = "10") int size,
                                   @RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "name") String sortBy,
                                   @RequestParam(required = false, defaultValue = "ASC") Sort.Direction direction
    ) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.by(direction, sortBy));
        return service.getAllGuests(pageRequest, name, country, email);
    }

    @GetMapping("/{id}")
    public GuestDto getGuestById(@PathVariable("id") Long id) {
        return service.getGuestDtoById(id);
    }

    @PostMapping
    public Guest createGuest(@RequestBody @Valid GuestDto guest) {
        return service.createGuest(guest);
    }

    @PutMapping("/{id}")
    public Guest updateGuest(@PathVariable("id") Long id, @RequestBody @Valid GuestDto dto) {
        return service.updateGuest(id, dto);
    }

    @DeleteMapping("/{id}")
    public void deleteGuest(@PathVariable("id") Long id) {
        service.deleteGuest(id);
    }

    @PostMapping("/{id}/rooms/{roomId}/bookings")
    public Booking bookRoom(@PathVariable Long id,
                            @PathVariable Long roomId,
                            @RequestParam Long startDate,
                            @RequestParam Long endDate) {
        return service.bookRoom(id, roomId, startDate, endDate);
    }

    @DeleteMapping("/{id}/bookings/{bookingId}")
    public void cancelBooking(@PathVariable Long id,
                                 @PathVariable Long bookingId) {
        service.cancelBooking(id, bookingId);
    }
}
