package com.example.airbnb.mapper;

import com.example.airbnb.dto.RoomDto;
import com.example.airbnb.entity.Room;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RoomMapper implements Mapper<Room, RoomDto> {
    @Override
    public RoomDto toDto(Room entity) {
        RoomDto dto = new RoomDto();
        dto.setId(entity.getId());
        dto.setOwner(entity.getOwner());
        dto.setPrice(entity.getPrice());
        dto.setRoomType(entity.getRoomType());
        dto.setSummary(entity.getSummary());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setHotTub(entity.isHotTub());
        dto.setTotalBathrooms(entity.getTotalBathrooms());
        dto.setUpdatedAt(entity.getUpdatedAt());
        dto.setTotalOccupancy(entity.getTotalOccupancy());
        return dto;
    }

    @Override
    public Room toEntity(RoomDto dto) {
        Room entity = new Room();
        entity.setPrice(dto.getPrice());
        entity.setRoomType(dto.getRoomType());
        entity.setSummary(dto.getSummary());
        entity.setHotTub(dto.isHotTub());
        entity.setTotalBathrooms(dto.getTotalBathrooms());
        entity.setTotalOccupancy(dto.getTotalOccupancy());
        entity.setCreatedAt(new Date());
        entity.setUpdatedAt(new Date());
        return entity;
    }

    @Override
    public Room updateEntity(Room entity, RoomDto dto) {
        entity.setPrice(dto.getPrice());
        entity.setRoomType(dto.getRoomType());
        entity.setSummary(dto.getSummary());
        entity.setHotTub(dto.isHotTub());
        entity.setTotalBathrooms(dto.getTotalBathrooms());
        entity.setTotalOccupancy(dto.getTotalOccupancy());
        entity.setUpdatedAt(new Date());
        return entity;
    }
}
