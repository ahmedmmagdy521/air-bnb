package com.example.airbnb.mapper;

public interface Mapper<E, D>{

    public D toDto(E entity) ;

    public E toEntity(D dto) ;
    public E updateEntity(E entity, D dto);
}
