package com.example.airbnb.mapper;

import com.example.airbnb.dto.GuestDto;
import com.example.airbnb.entity.Guest;
import org.springframework.stereotype.Component;

@Component
public class GuestMapper implements Mapper<Guest, GuestDto> {

    @Override
    public GuestDto toDto(Guest entity) {
        GuestDto dto = new GuestDto();
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setCountry(entity.getCountry());
        dto.setBio(entity.getBio());
        dto.setId(entity.getId());
        return dto;
    }

    @Override
    public Guest toEntity(GuestDto dto) {
        Guest guest = new Guest();
        guest.setName(dto.getName().trim());
        guest.setEmail(dto.getEmail().trim());
        guest.setCountry(dto.getCountry().trim());
        guest.setBio(dto.getBio().trim());
        return guest;
    }

    @Override
    public Guest updateEntity(Guest entity, GuestDto dto) {
        entity.setName(dto.getName().trim());
        entity.setCountry(dto.getCountry().trim());
        entity.setBio(dto.getBio().trim());
        return entity;
    }
}
