package com.example.airbnb.dto;

import com.example.airbnb.entity.Guest;
import com.example.airbnb.enums.RoomType;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Guest owner;
    @NotNull
    private Double price;
    private boolean hotTub;
    @NotNull
    private RoomType roomType;
    @NotNull
    private Integer totalOccupancy;
    @NotNull
    private Integer totalBathrooms;
    @NotEmpty
    private String summary;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createdAt;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date updatedAt;
}
