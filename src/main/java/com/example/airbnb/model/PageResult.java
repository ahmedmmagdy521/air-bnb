package com.example.airbnb.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PageResult {
    List result;
    Long totalCount;
}
